use std::f32::consts::E;
use std::fmt::{Display, Formatter};
use lifx_core::HSBK;
use crate::color::convert::{HCVColor, HSVColor};

mod convert;

#[derive(Copy, Clone)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

impl From<(u8, u8, u8)> for Color {
    fn from(rgb8: (u8, u8, u8)) -> Self {
        Self {
            r: rgb8.0 as f32 / u8::MAX as f32,
            g: rgb8.1 as f32 / u8::MAX as f32,
            b: rgb8.2 as f32 / u8::MAX as f32,
        }
    }
}

impl Into<(u8, u8, u8)> for Color {
    fn into(self) -> (u8, u8, u8) {
        (
            (self.r * u8::MAX as f32) as u8,
            (self.g * u8::MAX as f32) as u8,
            (self.b * u8::MAX as f32) as u8,
        )
    }
}

impl From<(u16, u16, u16)> for Color {
    fn from(rgb16: (u16, u16, u16)) -> Self {
        Self {
            r: rgb16.0 as f32 / u16::MAX as f32,
            g: rgb16.1 as f32 / u16::MAX as f32,
            b: rgb16.2 as f32 / u16::MAX as f32,
        }
    }
}

impl From<(f32, f32, f32)> for Color {
    fn from(rgb01: (f32, f32, f32)) -> Self {
        Self {
            r: rgb01.0,
            g: rgb01.1,
            b: rgb01.2,
        }
    }
}

impl Color {
    #[allow(non_snake_case)]
    pub fn sRGB(self) -> (f32, f32, f32) {
        (
            self.r.powf(0.46226525728f32),
            self.g.powf(0.46226525728f32),
            self.b.powf(0.46226525728f32),
        )
    }

    // Ported from https://gist.github.com/SaintWacko/803ef2abe8c835f7c886c0f6df726baf
    pub fn kelvin(self) -> u16 {
        let (r, g, b) = self.sRGB();

        // Wide RGB D65 https://gist.github.com/popcorn245/30afa0f98eea1c2fd34d
        let x0 = r * 0.649926 + g * 0.103455 + b * 0.197109;
        let y0 = r * 0.234327 + g * 0.743075 + b * 0.022598;
        let z0 = r * 0.000000 + g * 0.053077 + b * 1.035763;

        let x = x0 / (x0 + y0 + z0);
        let y = y0 / (x0 + y0 + z0);
        let mut n = (x - 0.3366) / (y - 0.1735);

        let mut cct = (-949.86315 + 6253.80338 * E.powf(-n / 0.92159))
            + (28.70599 * E.powf(-n / 0.20039))
            + (0.00004 * E.powf(-n / 0.07125));

        if cct > 50000f32 {
            n = (x - 0.3356) / (y - 0.1691);
        }

        if cct > 50000f32 {
            cct = 6284.48953 + 0.00228 * E.powf(-n / 0.07861)
                + (5.4535 * 10f32.powf(-36f32)) * E.powf(-n / 0.01543);
        }

        cct as u16
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "RGB({}, {}, {})", self.r, self.g, self.b)
    }
}

impl Into<HSBK> for Color {
    fn into(self) -> HSBK {
        let hsv: HSVColor = self.into();

        HSBK {
            hue: (hsv.h * 65535f32) as u16,
            saturation: (hsv.s * 65535f32) as u16,
            brightness: (hsv.v * 65535f32) as u16,
            kelvin: self.kelvin(),
        }
    }
}

impl Into<HCVColor> for Color {
    // Ported from https://www.shadertoy.com/view/4dKcWK
    fn into(self) -> HCVColor {
        let (r, g, b) = self.sRGB();

        let p = if g < b {
            (b, g, -1f32, 2f32 / 3f32)
        } else {
            (g, b, 0f32, -1f32 / 3f32)
        };

        let q = if r < p.0 {
            (p.0, p.1, p.3, r)
        } else {
            (r, p.1, p.2, p.0)
        };

        let c = q.0 - q.1.min(q.3);
        let h = ((q.3 - q.1) / (6f32 * c + 1e-10f32) + q.2).abs();

        HCVColor { h, c, v: q.0 }
    }
}

impl Into<HSVColor> for Color {
    fn into(self) -> HSVColor {
        let hcv: HCVColor = self.into();
        return hcv.into();
    }
}
