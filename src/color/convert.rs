#[derive(Copy, Clone)]
pub struct HCVColor {
    pub h: f32,
    pub c: f32,
    pub v: f32,
}

#[derive(Copy, Clone)]
pub struct HSVColor {
    pub h: f32,
    pub s: f32,
    pub v: f32,
}

impl Into<HSVColor> for HCVColor {
    fn into(self) -> HSVColor {
        let s = self.c / (self.v + 1e-10f32);
        HSVColor {h: self.h, s, v: self.v}
    }
}

#[derive(Copy, Clone)]
pub struct HSLColor {
    pub h: f32,
    pub s: f32,
    pub l: f32,
}

impl Into<HSLColor> for HCVColor {
    fn into(self) -> HSLColor {
        let l = self.v - self.c / 2f32;
        let s = self.c / (1f32 - (l * 2f32 - 1f32).abs() + 1e-10f32);
        HSLColor {h: self.h, s, l}
    }
}
