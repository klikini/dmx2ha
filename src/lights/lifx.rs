use crate::config::{LIFXConfig, LightPlacement};
use crate::lights::LightCommand;
use lifx_core::{BuildOptions, Message, RawMessage};
use log::{error, info};
use std::collections::HashMap;
use std::net::{Ipv4Addr, UdpSocket};
use std::process::exit;

pub struct Lifx {
    socket: UdpSocket,
    lights: HashMap<LightPlacement, LifxLight>,
    transition: u32,
}

struct LifxLight {
    ip: Ipv4Addr,
}

impl Lifx {
    pub fn init(config: &LIFXConfig) -> Self {
        let socket = UdpSocket::bind("0.0.0.0:56700").unwrap_or_else(|e| {
            error!("Failed to bind to UDP port: {:?}", e);
            exit(3);
        });

        let mut lights = HashMap::new();

        for (ip, placement) in config.lights.iter() {
            lights.insert(*placement, LifxLight { ip: *ip });
        }

        info!("LIFX initialized");

        Self { socket, lights, transition: config.transition }
    }

    fn send(&self, light: &LifxLight, message: Message) {
        let packet = RawMessage::build(&BuildOptions::default(), message)
            .unwrap()
            .pack()
            .unwrap();

        self.socket
            .send_to(&packet, light.ip.to_string() + ":56700")
            .unwrap_or_else(|e| {
                error!("Failed to send LIFX message: {:?}", e);
                exit(4);
            });
    }

    pub fn set_light(&self, command: LightCommand) {
        if let Some(light) = self.lights.get(&command.light) {
            self.send(
                light,
                Message::LightSetColor {
                    reserved: 0,
                    color: command.color.into(),
                    duration: self.transition,
                },
            );
        }
    }
}
