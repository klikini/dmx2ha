use crate::config::LightPlacement;
use crate::lights::lifx::Lifx;
use crate::lights::mqtt::Mqtt;
use crate::Config;
use crate::color::Color;
use log::{warn};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::{thread};

mod lifx;
mod mqtt;

#[derive(Copy, Clone)]
pub struct LightCommand {
    pub light: LightPlacement,
    pub color: Color,
}

pub fn start_handling(config: &Config) -> Sender<LightCommand> {
    let (sender, receiver): (Sender<LightCommand>, Receiver<LightCommand>) = channel();
    let lifx_handler = config.lifx.as_ref().map(Lifx::init);
    let mut mqtt_handler = config.mqtt.as_ref().map(Mqtt::init);

    thread::spawn(move || {
        loop {
            match receiver.recv() {
                Ok(command) => {
                    if let Some(lifx) = &lifx_handler {
                        lifx.set_light(command);
                    }

                    if let Some(mqtt) = &mut mqtt_handler {
                        mqtt.set_light(command);
                    }
                }
                Err(e) => {
                    warn!("Error receiving message on light thread: {}", e);
                    continue;
                }
            }
        }
    });

    sender
}
