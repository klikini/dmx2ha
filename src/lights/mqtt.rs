use crate::config::{MQTTConfig, LightPlacement};
use crate::lights::LightCommand;
use log::{error, info, warn};
use std::collections::HashMap;
use std::process::exit;
use std::time::Instant;
use paho_mqtt::{Client, Message, QOS_0, ConnectOptions};
use crate::lights::mqtt::MqttType::{Tasmota, Zigbee2MQTT};


pub struct Mqtt {
    client: Client,
    lights: HashMap<LightPlacement, MqttLight>,
    throttle: u16,
}

struct MqttLight {
    name: String,
    topic: String,
    comm_type: MqttType,
    last_msg: Option<Instant>,
}

enum MqttType {
    Zigbee2MQTT,
    Tasmota,
}

impl Mqtt {
    pub fn init(config: &MQTTConfig) -> Self {
        let client = Client::new(config.broker.to_string()).unwrap_or_else(|error| {
            error!("Error creating MQTT client: {:?}", error);
            exit(20);
        });

        if let Err(e) = client.connect(ConnectOptions::default()) {
            error!("Failed to connect to MQTT broker: {:?}", e);
            exit(20);
        }

        let mut lights = HashMap::new();

        if let Some(tasmota_config) = &config.tasmota {
            for (name, placement) in tasmota_config.lights.iter() {
                let cmnd_topic = format!("{}/{}", tasmota_config.base_topic, name);

                lights.insert(*placement, MqttLight {
                    name: name.to_string(),
                    topic: cmnd_topic.clone() + "/Color",
                    comm_type: Tasmota,
                    last_msg: None,
                });
            }

            info!("MQTT (Tasmota) initialized");
        }

        if let Some(z2m_config) = &config.zigbee2mqtt {
            for (name, placement) in z2m_config.lights.iter() {
                lights.insert(*placement, MqttLight {
                    name: name.to_string(),
                    topic: format!("{}/{}/set", z2m_config.base_topic, name),
                    comm_type: Zigbee2MQTT,
                    last_msg: None,
                });
            }

            info!("MQTT (Zigbee2MQTT) initialized");
        }

        Self { client, lights, throttle: config.throttle }
    }

    pub fn set_light(&mut self, command: LightCommand) {
        if let Some(light) = self.lights.get_mut(&command.light) {
            if self.throttle > 0 {
                let now = Instant::now();

                if let Some(last_msg) = light.last_msg {
                    if now.duration_since(last_msg).as_millis() < self.throttle as u128 {
                        return;
                    }
                }

                light.last_msg = Some(now);
            }

            let (r, g, b): (u8, u8, u8) = command.color.into();
            let color = format!("{},{},{}", r, g, b);

            let payload: Vec<u8> = match light.comm_type {
                Zigbee2MQTT => format!("{{\"color\":{{\"rgb\":\"{}\"}}}}", color).into_bytes(),
                Tasmota => color.into_bytes(),
            };

            let message = Message::new(&light.topic, payload, QOS_0);

            if let Err(e) = self.client.publish(message) {
                warn!("Error sending MQTT message to '{}': {:?}", light.name, e);
            }
        }
    }
}
