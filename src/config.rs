use serde_derive::Deserialize;
use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::hash::{Hash, Hasher};
use std::net::Ipv4Addr;

#[derive(Deserialize, Copy, Clone)]
pub struct LightPlacement {
    pub universe: u16,
    pub channel: u16,
}

impl Eq for LightPlacement {}

impl PartialEq<Self> for LightPlacement {
    fn eq(&self, other: &Self) -> bool {
        self.channel == other.channel && self.universe == other.universe
    }
}

impl Hash for LightPlacement {
    fn hash<H: Hasher>(&self, state: &mut H) {
        state.write_u16(self.universe);
        state.write_u16(self.channel);
    }
}

impl Display for LightPlacement {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<universe {}, channel {}>", self.universe, self.channel)
    }
}

#[derive(Deserialize)]
pub struct Config {
    pub lifx: Option<LIFXConfig>,
    pub zigbee2mqtt: Option<Zigbee2MQTTConfig>,
    pub mqtt: Option<MQTTConfig>,
    pub strict_packet_order: bool,
}

#[derive(Deserialize)]
pub struct LIFXConfig {
    pub lights: HashMap<Ipv4Addr, LightPlacement>,
    pub transition: u32,
}

#[derive(Deserialize)]
pub struct MQTTConfig {
    pub broker: String,
    pub throttle: u16,
    pub tasmota: Option<TasmotaMQTTConfig>,
    pub zigbee2mqtt: Option<Zigbee2MQTTConfig>,
}

#[derive(Deserialize)]
pub struct TasmotaMQTTConfig {
    pub base_topic: String,
    pub lights: HashMap<String, LightPlacement>,
}

#[derive(Deserialize)]
pub struct Zigbee2MQTTConfig {
    pub base_topic: String,
    pub lights: HashMap<String, LightPlacement>,
}
