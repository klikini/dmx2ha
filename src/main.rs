mod config;
mod dmx;
mod lights;
mod color;

use std::env::args;
use std::fs::{read, write};
use std::path::Path;
use std::process::exit;
use std::thread;
use std::time::Duration;

use crate::config::Config;
use crate::dmx::start_listening;
use crate::lights::{start_handling};
use log::{error, info, LevelFilter};

const DEFAULT_CONFIG: &str = "strict_packet_order = true

[lifx]
transition = 1 # ms

    [lifx.lights]
    \"192.168.2.123\" = { universe = 0, channel = 1 }
    \"192.168.2.124\" = { universe = 0, channel = 7 }
    \"192.168.2.125\" = { universe = 0, channel = 13 }

[mqtt]
broker = \"192.168.2.10:1883\"
throttle = 500 # ms

    [mqtt.zigbee2mqtt]
        base_topic = \"zigbee2mqtt\"

        [mqtt.zigbee2mqtt.lights]
        \"Bathroom\" = { universe = 6, channel = 145 }
        \"Kitchen Sink\" = { universe = 6, channel = 151 }

    [mqtt.tasmota]
    base_topic = \"cmnd\"

        [mqtt.tasmota.lights]
        \"bedside_lamp\" = { universe = 5, channel = 1 }
";

fn main() {
    env_logger::builder()
        .filter_level(LevelFilter::Debug)
        .filter_module("paho_mqtt", LevelFilter::Info)
        .init();

    if args().len() != 1 {
        error!("dmx2ha takes no arguments");
        exit(1);
    }

    let config_path: &Path = Path::new("dmx2ha.toml");

    if !config_path.exists() {
        if let Err(e) = write(config_path, DEFAULT_CONFIG) {
            error!("Error creating default dmx2ha.toml: {}", e);
            exit(2);
        }

        info!("dmx2ha.toml did not exist, so a default config file has been created. Fill it in and try again.");
        exit(0);
    }

    // Parse config file
    let config_str = read(config_path).unwrap_or_else(|e| panic!("Failed to read config: {}", e));
    let config: Config =
        toml::from_slice(&config_str).unwrap_or_else(|e| panic!("Failed to parse config: {}", e));

    let sender = start_handling(&config);
    start_listening(sender, config.strict_packet_order);

    loop {
        thread::sleep(Duration::from_secs(1));
    }
}
