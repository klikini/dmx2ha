use crate::config::LightPlacement;
use crate::lights::{LightCommand};
use crate::color::Color;
use log::{error, warn, info};
use std::cmp::{min, Ordering};
use std::net::UdpSocket;
use std::process::exit;
use std::sync::mpsc::Sender;
use std::thread;

const ART_NET_HEADER: [u8; 8] = [b'A', b'r', b't', b'-', b'N', b'e', b't', b'\0'];

pub fn start_listening(sender: Sender<LightCommand>, strict_packet_order: bool) {
    let socket = UdpSocket::bind("0.0.0.0:6454").unwrap_or_else(|e| {
        error!("Error binding to UDP port 6454: {}", e);
        exit(4);
    });

    info!("Listening for Art-Net data on UDP port 6454 on all interfaces");

    thread::Builder::new()
        .name("UDP listener".to_string())
        .spawn(move || {
            let mut buf = [0; 1024];
            let mut current_sequence = 0u8;

            loop {
                let receive_result = socket.recv_from(&mut buf);

                if let Err(e) = receive_result {
                    warn!("Error receiving UDP data: {}", e);
                    continue;
                }

                let (n, from) = receive_result.unwrap();

                if n < ART_NET_HEADER.len() {
                    warn!(
                        "Art-Net packet from {} is smaller than the expected header",
                        from
                    );
                    continue;
                }

                // https://en.wikipedia.org/wiki/Art-Net#Packet_format
                if !buf.starts_with(&ART_NET_HEADER) {
                    warn!("Packet from {} was not Art-Net", from);
                    continue;
                }

                if u16::from_le_bytes([buf[8], buf[9]]) != 0x5000 {
                    // We only care about DMX data packets
                    continue;
                }

                if buf[10] != 0 || buf[11] != 14 {
                    warn!(
                        "Art-Net packet from {} had invalid version: expected 0.14 but got {}.{}",
                        from, buf[10], buf[11]
                    );
                    continue;
                }

                if strict_packet_order {
                    // Sequence field ranges 0x01..0xFF.
                    // 0x00 means the sender isn't using this feature.
                    if current_sequence == 0xFF {
                        current_sequence = 0x01;
                    } else {
                        current_sequence += 0x01;
                    }

                    let packet_sequence = buf[12];

                    if packet_sequence != 0x00 {
                        match packet_sequence.cmp(&current_sequence) {
                            Ordering::Less => {
                                warn!("Ignoring packet #{} because we are expecting packet #{}", packet_sequence, current_sequence);
                                continue;
                            }
                            Ordering::Equal => {}
                            Ordering::Greater => {
                                warn!("Skipping to packet #{} but we were expecting packet #{}", packet_sequence, current_sequence);
                                current_sequence = packet_sequence;
                            }
                        }
                    }
                }

                // The "physical" field is ignored and I don't plan on ever needing it.

                let universe = u16::from_le_bytes([buf[14], buf[15]]);
                let length = u16::from_be_bytes([buf[16], buf[17]]);

                for chan in (0..min(length, buf.len() as u16 - 18u16)).step_by(6) {
                    let placement: LightPlacement = LightPlacement {
                        universe,
                        channel: chan + 1,
                    };

                    let r = u16::from_be_bytes([buf[(chan + 18) as usize], buf[(chan + 19) as usize]]);
                    let g = u16::from_be_bytes([buf[(chan + 20) as usize], buf[(chan + 21) as usize]]);
                    let b = u16::from_be_bytes([buf[(chan + 22) as usize], buf[(chan + 23) as usize]]);

                    sender
                        .send(LightCommand {
                            light: placement,
                            color: Color::from((r, g, b)),
                        }).unwrap_or_else(|e| {
                        panic!(
                            "Error sending light command from {} to light thread: {}",
                            from, e
                        );
                    });
                }
            }
        })
        .unwrap_or_else(|e| {
            error!("Failed to spawn UDP listener thread: {}", e);
            exit(4);
        });
}
