# dmx2ha

Receives Art-Net packets and sends color commands to:

- LIFX bulbs
- Tasmota lights (through any MQTT broker)
- Zigbee lights (through Zigbee2MQTT)

## Download the latest release

[Linux](https://gitlab.com/klikini/dmx2ha/-/jobs/artifacts/main/raw/target/release/dmx2ha?job=build)

### Or build it yourself

1. Make sure [`cargo`](https://rustup.rs/) and [`cmake`](https://cmake.org/) are installed
2. Run `cargo build --release`

## Configuration

The configuration is read from a file named `dmx2ha.toml` in the working directory.
If it doesn't exist, an example will be generated on the first run.

### Structure

- strict_packet_order (boolean) - drop out-of-order packets. Recommended value is `true` unless you see too many warning about dropped packets.
- lifx
  - transition (integer) - transition time (in milliseconds) for each command
  - lights - map IP address to universe/channel
- mqtt
  - broker (string) - broker IP/hostname and port
  - throttle (integer) - minimum time (in milliseconds) to wait between sending commands to a single light
  - zigbee2mqtt
    - base_topic (string) - base topic configured in Zigbee2MQTT
    - lights - map friendly name to universe/channel
  - tasmota
    - base_topic - Tasmota command topic
    - lights - map MQTT topic to universe/channel

### Example

```toml
strict_packet_order = true

[lifx]
transition = 1 # ms

    [lifx.lights]
    "192.168.2.123" = { universe = 0, channel = 1 }
    "192.168.2.124" = { universe = 0, channel = 7 }
    "192.168.2.125" = { universe = 0, channel = 13 }

[mqtt]
broker = "192.168.2.10:1883"
throttle = 500 # ms
    
    [mqtt.zigbee2mqtt]
        base_topic = "zigbee2mqtt"
    
        [mqtt.zigbee2mqtt.lights]
        "Bathroom" = { universe = 6, channel = 145 }
        "Kitchen Sink" = { universe = 6, channel = 151 }
    
    [mqtt.tasmota]
    base_topic = "cmnd"
    
        [mqtt.tasmota.lights]
        "bedside_lamp" = { universe = 5, channel = 1 }
```
